---
title: "À PROPOS DE MOI"
description: "This is meta description."
author_image : "images/about/"
author_signature : "images/about/signature.png"
draft: false
---

Bonjour, je m’appelle Mouslim, j’ai 23 ans et je suis en formation au Passe Numérique Pro dans la spécialité Technicien DevOps au Conservatoire National des Arts et Métiers de Paris. 

Je suis, depuis toujours, fasciné par le milieu de l’informatique. Après avoir essayé une autre voie, la prothèse dentaire, je me suis rendu compte que c’est vraiment dans le milieu de l’informatique que je me sens le mieux.